from django.contrib import admin
from django.urls import path,include
from red.api.urls import urlpatterns

urlpatterns = [
    path('red/', include(urlpatterns))
]