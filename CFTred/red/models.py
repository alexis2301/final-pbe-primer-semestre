from django.db import models

# Create your models here.

class Usuario(models.Model):
    Nombre = models.TextField(30)    
    Contraseña = models.TextField(25)

    def __str__(self):
        return self.body

class Topico(models.Model):
    Titulo = models.CharField(max_length=25)
    Contenido = models.TextField()    

    def __str__(self):
        return self.body

class Post(models.Model):
    Titulo = models.CharField(max_length=50)
    Contenido = models.TextField()   
    Fecha = models.DateTimeField()   

    def __str__(self):
        return self.body