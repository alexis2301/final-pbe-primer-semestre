from django.urls import path
from . import views

urlpatterns = [
    ######## Usuario ##############
    path('addUsuario/', views.addUsuario),
    path('getUsuario/', views.getUsuario),
    path('updateUsuario/', views.updateUsuario),
    path('deleteUsuario/', views.deleteUsuario),
    ######## Topico ###############
    path('addTopico/', views.addTopico),
    path('getTopico/', views.getTopico),
    path('updateTopico/', views.updateTopico),
    path('deleteTopico/', views.deleteTopico),
    ######## Post #################
    path('addPost/', views.addPost),
    path('getPost/', views.getPost),
    path('updatePost/', views.updatePost),
    path('deletePost/', views.deletePost),
]