from red.models import Usuario, Topico, Post
from red.api.serializers import UsuarioSerializer, TopicoSerializer, PostSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view

###############Para usuario###################
@api_view(['POST'])
def addUsuario(request):
    data = request.data
    usuario = Usuario.objects.create(
        Nombre = data['Nombre'],
        Contraseña = data['Contraseña']
    )
    serializer = UsuarioSerializer(usuario , many = False)
    return Response(serializer.data)

@api_view(['GET'])
def getUsuario(recuest):
    usuario = Usuario.objects.all()
    serializer = UsuarioSerializer(usuario , many = True)
    return Response(serializer.data)

@api_view(['PUT'])
def updateUsuario(request):
    print(request)
    data = request.data
    usuario = Usuario.objects.get(id = data['id'])
    serializer = UsuarioSerializer(instance = post, data = data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def  deleteUsuario(request):
    data = request.data
    usuario= Usuario.objects.filter(id = data['id'])
    usuario.delete()
    respuesta = {'message': 'Eliminación exitosa'}
    return Response(respuesta)
###############Para topico###################

@api_view(['POST'])
def addTopico(request):
    data = request.data
    topico = Topico.objects.create(
        Titulo = data['Titulo'],
        Contenido = data['Contenido']
    )
    serializer = TopicoSerializer(topico , many = False)
    return Response(serializer.data)

@api_view(['GET'])
def getTopico(recuest):
    topico = Topico.objects.all()
    serializer = TopicoSerializer(topico , many = True)
    return Response(serializer.data)

@api_view(['PUT'])
def updateTopico(request):
    print(request)
    data = request.data
    topico = Topico.objects.get(id = data['id'])
    serializer = TopicoSerializer(instance = post, data = data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def  deleteTopico(request):
    data = request.data
    topico = Topico.objects.filter(id = data['id'])
    topico.delete()
    respuesta = {'message': 'Eliminación exitosa'}
    return Response(respuesta)
###############Para Post###################
@api_view(['POST'])
def addPost(request):
    data = request.data
    post = Post.objects.create(
        Titulo = data['Titulo'],
        Contenido = data['Contenido'],
        Fecha = data['Fecha']
    )
    serializer = PostSerializer(post , many = False)
    return Response(serializer.data)

@api_view(['GET'])
def getPost(recuest):
    post = Post.objects.all()
    serializer = PostSerializer(post , many = True)
    return Response(serializer.data)

@api_view(['PUT'])
def updatePost(request):
    print(request)
    data = request.data
    post = Post.objects.get(id = data['id'])
    serializer = PostSerializer(instance = post, data = data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def  deletePost(request):
    data = request.data
    post = Post.objects.filter(id = data['id'])
    post.delete()
    respuesta = {'message': 'Eliminación exitosa'}
    return Response(respuesta)