from rest_framework.serializers import ModelSerializer
from red.models import Usuario, Topico, Post

class UsuarioSerializer(ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id','Nombre','Contraseña']

class TopicoSerializer(ModelSerializer):
    class Meta:
        model = Topico
        fields = ['id','Titulo','Contenido']

class PostSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ['id','Titulo','Contenido','Fecha']