from django.contrib import admin
from red.models import Usuario, Topico, Post

# Register your models here.
@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    lis_display = ['id','Nombre','Contraseña']

@admin.register(Topico)
class TopicoAdmin(admin.ModelAdmin):
    lis_display = ['id','Titulo','Contenido']

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    lis_display = ['id','Titulo','Contenido','Fecha']